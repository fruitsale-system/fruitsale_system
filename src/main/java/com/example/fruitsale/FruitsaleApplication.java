package com.example.fruitsale;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@MapperScan("com.example.fruitsale.mapper")//记得加这句
@EnableScheduling  //开启定时任务
public class FruitsaleApplication {

    public static void main(String[] args) {
        SpringApplication.run(FruitsaleApplication.class, args);
    }

}
