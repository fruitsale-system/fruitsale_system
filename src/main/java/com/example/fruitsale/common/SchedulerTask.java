package com.example.fruitsale.common;


//import com.qtt.app.service.AppViewsRecordService;//这个是企团团的
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

//@Slf4j
@Component
@Async// 可加在类或方法，开启异步事件的支持
public class SchedulerTask{
//    @Autowired
    //企团团后端写的定期删除浏览记录的类
//    AppViewsRecordService appViewsRecordService;
//    Logger log = LoggerFactory.getLogger(SchedulerTask.class);
//    @Scheduled(cron = "0 */1 * * * ?")//每天1点开启定时任务
////    @Scheduled(cron = "*/5 * * * * ?")
//    public void scheduled(){
//        log.info("使用cron: {}");
//        Date newDate=new Date();
//        LocalDate deleteDate=getDate(newDate,3);
//        try{
//            System.out.println("deleteDate="+deleteDate);
//            appViewsRecordService.deleteSchedule(deleteDate+" 00:00:00");
//        }catch (Exception e){
//            System.out.println("deleteFail");
////            logger.error("删除30天前的扬尘数据失败！"+e.getMessage());
//        }
//
//    }


    //默认的几个函数
//    @Scheduled(fixedRate = 5000)
//    public void scheduled1() {
//        log.info("使用fixedRate {}");
//    }
//
//    @Scheduled(fixedDelay = 5000)
//    public void scheduled2() {
//        log.info("使用fixedDelay {}");
//    }
//    @Scheduled(initialDelay = 10000,fixedRate = 6000)
//    public void scheduled3() {
//        log.info("使用initialDelay {}");
//    }

    public static LocalDate getDate(Date now, int days){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.DATE, -days);
        Date deleteDate=calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString=sdf.format(deleteDate);
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate=LocalDate.parse(dateString,fmt);
        return localDate;
    }
}