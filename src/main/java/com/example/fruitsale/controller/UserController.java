package com.example.fruitsale.controller;

import com.example.fruitsale.common.ApiResponse;
import com.example.fruitsale.entity.UserManage;
import com.example.fruitsale.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Api(tags = "UserController", description = "用户管理")   //tags是api名字，description是简介，value:已废用
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @ApiOperation(value = "根据id查找一条记录", notes = "")
    //注意是@PathVariable 不是@PathParam ；这个是路径传值
    @GetMapping("/list/{userId}")
    public ApiResponse<UserManage> getUserById(@PathVariable("userId") Integer userId){
        UserManage entity = userService.getById(userId);

        if (entity != null){
            return ApiResponse.<UserManage>builder().code(200).message("查找成功").data(entity).build();
        }
        else {
            return ApiResponse.<UserManage>builder().code(400).message("查找失败").data(null).build();
        }

    }

    @ApiOperation(value = "添加一条新的记录", notes = "")
    @PostMapping("/list")
    public ApiResponse<String> addMsg(@ApiParam(value="",required = true) @RequestBody(required=true) UserManage requestData){
        UserManage entity = new UserManage();
        entity.setCardUid(requestData.getCardUid());
        entity.setOpenid(requestData.getOpenid());
        entity.setNickname(requestData.getNickname());
        entity.setRealname(requestData.getRealname());
        entity.setHeadImage(requestData.getHeadImage());
        entity.setGender(requestData.getGender());
        entity.setGmtCreate(new Date());
        System.out.println("datetime="+ entity.getGmtCreate());
//        entity.setCardMoney(requestData.getSoldCourseSum());

        int b = (userService.addUser(entity));
        if (b == 1){
            return ApiResponse.<String>builder().code(200).message("添加成功").data("success").build();
        }
        else {
            return ApiResponse.<String>builder().code(400).message("添加失败").data("fail").build();
        }
    }

    @ApiOperation(value = "修改一条记录", notes = "只传需要修改的参数.")
    @PutMapping("/list/{userId}")
    public ApiResponse<String> updateUserByIdb(@ApiParam(value="用户id",required = true) @PathVariable("userId") Integer userId,
                                               @ApiParam(value="修改用户信息：不修改的就删掉，只传要修改的字段",required = true) @RequestBody(required=true) UserManage requestData){
        //先以userId往用户表查找，如果不存在，则无法修改；如果存在，在原来的记录上更新需要修改的字段即可。
        UserManage entity = userService.getById(userId);
        if (entity ==null){
            return ApiResponse.<String>builder().code(400).message("修改失败,输入的id有误，该用户不存在").data("fail").build();
        }
        System.out.println(entity);
        if((requestData.getCardUid())!=null ){
            entity.setCardUid(requestData.getCardUid());
        }
        if(requestData.getOpenid() != null){
            entity.setOpenid(requestData.getOpenid());
        }
        if(requestData.getCardMoney() != null){
            entity.setCardMoney(requestData.getCardMoney());
        }
        if(requestData.getGender() != null){
            entity.setGender(requestData.getGender());
        }
        if(requestData.getNickname() != null){
            entity.setNickname(requestData.getNickname());
        }
        if(requestData.getRealname() != null){
            entity.setRealname(requestData.getRealname());
        }

        Date gmtModified = new Date();
        entity.setGmtModified(gmtModified);
        Boolean b = userService.updateById(userId,entity);
        if (b == true){
            return ApiResponse.<String>builder().code(200).message("修改成功").data(b.toString()).build();
        }
        else {
            return ApiResponse.<String>builder().code(400).message("修改失败").data(b.toString()).build();
        }
    }

}
