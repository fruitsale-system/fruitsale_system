//package com.example.fruitsale.controller;
//
//import com.example.fruitsale.common.ApiResponse;
//import com.example.fruitsale.entity.MsgBoardEntity;
//import com.example.fruitsale.service.IndexService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//
//@Api(tags = "MsgController", description = "信息管理")   //tags是api名字，description是简介，value:已废用
//@RestController
//@RequestMapping("/Msg")
//public class IndexController {
//    @Autowired
//    IndexService indexService;
//
//    @ApiOperation(value = "查询所有", notes = "可以查看msg表中的所有数据")  //value是Get /Msg/list旁边的那个api简介；notes是点开的介绍，可用于详细说明api作用
//    @GetMapping("/list")
//    public ApiResponse<List<MsgBoardEntity>> listall(){
////    public List<MsgBoardEntity> listall(){
//        indexService.listAll();
//        List<MsgBoardEntity> entities = indexService.listAll();
////        return entities;
//        return ApiResponse.<List<MsgBoardEntity>>builder().code(200).message("查看成功").data(entities).build();
//    }
//
//    @ApiOperation(value = "添加一条新的记录", notes = "结构体里面的id，gmt_create都不用填，只需要传入author和msg。")
//    //@RequestBody 是json传值
//    @PostMapping("/list")
//    public ApiResponse<String> addMsg(@ApiParam("传入的是msg实体，id和gmt_create都不用填")@RequestBody MsgBoardEntity entity){
////    public String addMsg(@RequestBody MsgBoardEntity entity){
//        Boolean b = indexService.addMsg(entity);
//        if (b == true){
//            return ApiResponse.<String>builder().code(200).message("添加成功").data(b.toString()).build();
//        }
//        else {
//            return ApiResponse.<String>builder().code(400).message("添加失败").data(b.toString()).build();
//        }
//    }
//
//    @ApiOperation(value = "删除一条记录", notes = "哈哈哈")
//    //注意是@PathVariable 不是@PathParam ；这个是路径传值
//    @DeleteMapping("/list/{id}")
////    public String deleteMsg(@PathVariable("id") long id){
//    public ApiResponse<String> deleteMsg(@PathVariable("id") long id){
//        Boolean b = indexService.delete(id);
//
//        if (b == true){
//            return ApiResponse.<String>builder().code(200).message("删除成功").data(b.toString()).build();
//        }
//        else {
//            return ApiResponse.<String>builder().code(400).message("删除失败").data(b.toString()).build();
//        }
//    }
//
//    @ApiOperation(value = "修改一条记录", notes = "哈哈哈")
//    //注意是@PathVariable 不是@PathParam ；这个是路径传值
//    @PutMapping("/list/{id}")
//    public ApiResponse<String> updateMsg(@RequestParam("id") long id, @ApiParam("发送的信息")@RequestParam("msg") String msg){
//        Boolean b = indexService.update(id,msg);
//        if (b == true){
//            return ApiResponse.<String>builder().code(200).message("修改成功").data(b.toString()).build();
//        }
//        else {
//            return ApiResponse.<String>builder().code(400).message("修改失败").data(b.toString()).build();
//        }
//    }
//
//
//    @ApiOperation(value = "查询限定作者的信息", notes = "可以查看msg表中该作者的相关数据")  //value是Get /Msg/list旁边的那个api简介；notes是点开的介绍，可用于详细说明api作用
//    @GetMapping("/onelist")
//    public ApiResponse<List<MsgBoardEntity>> getlistByauthor(@RequestParam("author") String author){
////        indexService.getListByauthor(author);
//        List<MsgBoardEntity> entities = indexService.getListByauthor(author);
//
//        entities.get(0).setMsg("吃鸡");
////        entities.get(1).setMsg("打代码");
////        if (entities.get(0).getMsg() != "打代码"){
////            entities.get
////        }
////        return entities;
//        return ApiResponse.<List<MsgBoardEntity>>builder().code(200).message("查看成功").data(entities).build();
//    }
//}
