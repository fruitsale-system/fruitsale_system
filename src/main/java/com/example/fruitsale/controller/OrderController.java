package com.example.fruitsale.controller;

import com.example.fruitsale.common.ApiResponse;
import com.example.fruitsale.entity.Card;
import com.example.fruitsale.entity.Order;
import com.example.fruitsale.entity.UserManage;
import com.example.fruitsale.service.CardService;
import com.example.fruitsale.service.OrderService;
import com.example.fruitsale.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Api(tags = "OrderController", description = "订单管理")   //tags是api名字，description是简介，value:已废用
@RestController
@RequestMapping("orders")
public class OrderController {
    //每一个要单独写一个@Autowired，不然会报错
    @Autowired
    CardService cardService;
    @Autowired
    OrderService orderService;
    @Autowired
    UserService userService;

    @ApiOperation(value = "根据id查找一条记录", notes = "")
    //注意是@PathVariable 不是@PathParam ；这个是路径传值
    @GetMapping("/list/{id}")
    public ApiResponse<Order> getUserById(@PathVariable("id") Integer id){
        Order entity = orderService.getById(id);

        if (entity != null){
            return ApiResponse.<Order>builder().code(200).message("查找成功").data(entity).build();
        }
        else {
            return ApiResponse.<Order>builder().code(400).message("查找失败").data(null).build();
        }

    }
    @ApiOperation(value = "查看某个用户的线上、线上订单（1线上、2线下）", notes = "")  //value是Get /Msg/list旁边的那个api简介；notes是点开的介绍，可用于详细说明api作用
    @GetMapping("/OrderStateList/{userId}")
    public ApiResponse<List<Order>> listByIdAndState(@ApiParam(value="用户id",required = true) @PathVariable(value="userId",required=true) Integer userId,
                                                         @ApiParam(value="订单类型,1线上、2线下",required = true) @RequestParam(value="buyWay",required=true) Integer buyWay){
        List<Order> entities = orderService.getOrderByUseridAndBuyway(userId,buyWay);
        if (entities.size() ==0) {//注意列表如果返回的是空，需要这么判断，空列表为[]
            return ApiResponse.<List<Order>>builder().code(400).message("还没有此类订单").data(entities).build();
        }
        return ApiResponse.<List<Order>>builder().code(200).message("查看成功").data(entities).build();
    }

    @ApiOperation(value = "获取某个卡片的所有下单记录", notes = "")  //value是Get /Msg/list旁边的那个api简介；notes是点开的介绍，可用于详细说明api作用
    @GetMapping("/getOrderByCarduid")
    public ApiResponse<List<Order>> getOrderByCarduid(@ApiParam(value="卡uid",required = true) @RequestParam(value="cardUid",required=true) String cardUid){
        List<Order> entities = orderService.getOrderByCarduid(cardUid);
        if (entities.size() ==0) {//注意列表如果返回的是空，需要这么判断，空列表为[]
            return ApiResponse.<List<Order>>builder().code(400).message("还没有此类订单").data(entities).build();
        }
        return ApiResponse.<List<Order>>builder().code(200).message("查看成功").data(entities).build();
    }

    @ApiOperation(value = "查看某个用户的订单类型（1刷卡支付、2扫码支付）", notes = "注意要限定buyWay等于2即线下购买")  //value是Get /Msg/list旁边的那个api简介；notes是点开的介绍，可用于详细说明api作用
    @GetMapping("/getOrderByPaystate/{userId}")
    public ApiResponse<List<Order>> getOrderByPaystate(@ApiParam(value="用户id",required = true) @PathVariable(value="userId",required=true) Integer userId,
                                                     @ApiParam(value="订单类型,1刷卡支付、2扫码支付",required = true) @RequestParam(value="payWay",required=true) Integer payWay){
        Integer buyWay = 2;
        List<Order> entities = orderService.getOrderByUseridAndPayway(userId,payWay,buyWay);
        if (entities.size() ==0) {//注意列表如果返回的是空，需要这么判断，空列表为[]
            return ApiResponse.<List<Order>>builder().code(400).message("还没有此类订单").data(entities).build();
        }
        return ApiResponse.<List<Order>>builder().code(200).message("查看成功").data(entities).build();
    }

    @ApiOperation(value = "购买下单（线上购买支付、线下购买刷卡支付、线下购买扫码三种支付方式）", notes = "硬件端即线下，线下刷卡支付，需要传入buyWay值2，payWay值1，卡uid，水果名，水果单价，水果斤数这6个参数，其余参数删去；线下扫码支付，不需要在硬件端调用api接口，只需通过mqtt传给小程序相关数据即可,然后小程序调用完接口之后，会返回mqtt的topuc还剩多少钱；buyWay值1，payWay值2为线下扫码支付；buyWay值2，payWay值1为线上微信支付")  //value是Get /Msg/list旁边的那个api简介；notes是点开的介绍，可用于详细说明api作用
    @PostMapping("/payOrder")
    public ApiResponse<String> payOrder(@ApiParam(value="请求参数：buyWay为1表示线上购买，2表示线下购买；payWay为1表示刷卡购买，2表示扫码微信购买；不需要的参数可以删除",required = true) @RequestBody(required = true) Order requestData){

        Order entity =new Order();
        System.out.println("buyway="+requestData.getBuyWay()+",payway="+requestData.getPayWay()+",requestData="+requestData);
        //先判断是线上还是线下购买，1为线上，2为线下.线上小程序购买一定存微信openid，但不一定存卡片uid
        if(requestData.getBuyWay() == 1 ){
            entity.setBuyWay(1);
            entity.setPayWay(2);
            entity.setUserId(requestData.getUserId());
            entity.setFruitName(requestData.getFruitName());
            entity.setFruitWeight(requestData.getFruitWeight());
            entity.setUnitPrice(requestData.getUnitPrice());
            entity.setTotalPrice(requestData.getTotalPrice());
            entity.setPayState(1);//1表示已经支付，2表示暂未支付
            entity.setGmtCreate(new Date());
            System.out.println("线上entity ="+entity);
        }
        //如果是线下购买，且选择刷卡支付，1表示刷卡，2表示扫码微信支付，那么就会直接调用该方法生成一条记录。不存openid只存cardid
        if(requestData.getBuyWay() == 2 && requestData.getPayWay() == 1){
            entity.setBuyWay(2);
            entity.setPayWay(1);
            entity.setCardUid(requestData.getCardUid());
            entity.setFruitName(requestData.getFruitName());
            entity.setFruitWeight(requestData.getFruitWeight());
            entity.setUnitPrice(requestData.getUnitPrice());
            entity.setTotalPrice(requestData.getTotalPrice());
            entity.setPayState(1);//1表示已经支付，2表示暂未支付
            entity.setGmtCreate(new Date());
            Integer userId = cardService.getUseridByCarduid(requestData.getCardUid());
            entity.setUserId(userId);
            //修改card表中的卡片余额
            System.out.println("cardUid="+requestData.getCardUid());
            Card cardEntity = cardService.getByCarduid(requestData.getCardUid());
            Float totalPrice = requestData.getUnitPrice()*requestData.getFruitWeight();
            cardEntity.setCardMoney(cardEntity.getCardMoney()- requestData.getTotalPrice());
            cardService.updateCard(cardEntity);
        }
        //线下扫码支付.保存的是openid.这种方式不需要用到卡片，所以不需要扣钱
        if(requestData.getBuyWay() == 2 && requestData.getPayWay() == 2){
            entity.setBuyWay(2);
            entity.setPayWay(2);
            entity.setUserId(requestData.getUserId());
            entity.setFruitName(requestData.getFruitName());
            entity.setFruitWeight(requestData.getFruitWeight());
            entity.setUnitPrice(requestData.getUnitPrice());
            entity.setTotalPrice(requestData.getTotalPrice());
            entity.setPayState(1);//1表示已经支付，2表示暂未支付
            entity.setGmtCreate(new Date());
        }
        if(entity.getFruitName().equals("苹果")){
            entity.setFruitPicture("/image/apple.jpg");
        }
        if(entity.getFruitName().equals("桃子")){
            entity.setFruitPicture("/image/peach.jpg");
        }
        if(entity.getFruitName().equals("香蕉")){
            entity.setFruitPicture("/image/banana.jpg");
        }


        int payFlag = orderService.payOrder(entity);
        if (payFlag ==0) {
            return ApiResponse.<String>builder().code(400).message("添加订单失败").data(null).build();
        }
        return ApiResponse.<String>builder().code(200).message("添加订单成功").data(null).build();
    }


}
