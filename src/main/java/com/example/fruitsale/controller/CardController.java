package com.example.fruitsale.controller;

import com.example.fruitsale.common.ApiResponse;
import com.example.fruitsale.entity.Card;
import com.example.fruitsale.entity.ChargeRecord;
import com.example.fruitsale.entity.RequestData.BindUser;
import com.example.fruitsale.entity.UserManage;
import com.example.fruitsale.service.CardService;
import com.example.fruitsale.service.ChargeRecordService;
import com.example.fruitsale.service.IndexService;
import com.example.fruitsale.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static com.example.fruitsale.utils.Converter.getAsString;
import static com.example.fruitsale.utils.TypeCastUtil.toFloat;

@Api(tags = "CardController", description = "卡片管理")   //tags是api名字，description是简介，value:已废用
@RestController
@RequestMapping("/card")
public class CardController {
    @Autowired
    CardService cardService;
    @Autowired
    ChargeRecordService chargeRecordService;
    @Autowired
    UserService userService;


    @ApiOperation(value = "根据用户id查找该用户的所有卡片", notes = "")
    //注意是@PathVariable 不是@PathParam ；这个是路径传值
    @GetMapping("/list/{userId}")
    public ApiResponse<List<Card>> getCardByUserid(@PathVariable("userId") Integer userId){
        List<Card> entities = cardService.getMyCardsByUserid(userId);

        if (entities.size() != 0){
            return ApiResponse.<List<Card>>builder().code(200).message("查找成功").data(entities).build();
        }
        else {
            return ApiResponse.<List<Card>>builder().code(400).message("查找失败").data(null).build();
        }

    }

    @ApiOperation(value = "绑定卡片和微信用户", notes = "")
    @PostMapping("/bindUser")
//    @ApiParam(value="用户id",required = true) @RequestParam(value="userId",required=true) Integer userId,
//    @ApiParam(value="卡片uid",required = true) @RequestParam(value="cardUid",required=true) String cardUid
    public ApiResponse<String> bindUser(@ApiParam(value="",required = true) @RequestBody(required=true) BindUser requestData) {
        int flag;
        Integer userId = requestData.getUserId();
        String cardUid = requestData.getCardUid();
        //先判断该卡是否曾经被绑定过，且是否已经解绑
        UserManage userEntity = userService.getById(userId);
        String openid = userEntity.getOpenid();
        Card entity = cardService.getByCarduid(cardUid);
        if(entity!=null && entity.getIsBind()==1){
            return ApiResponse.<String>builder().code(401).message("绑定失败,该卡片已经被绑定").data("fail").build();
        }
        else if (entity !=null && entity.getIsBind() == 2){
            entity.setOpenid(openid);
            entity.setIsBind(1);//1为已绑定，2为未绑定
            entity.setGmtModified(new Date());
            flag = cardService.updateCard(entity);
        }
        else {
            entity = new Card();
            entity.setCardUid(cardUid);
            entity.setOpenid(openid);
            entity.setIsBind(1);//1为已绑定，2为未绑定
            entity.setCardMoney(toFloat(0));
            entity.setGmtCreate(new Date());
            flag = (cardService.cardBindUser(entity));
        }

        if (flag == 1){
            return ApiResponse.<String>builder().code(200).message("绑定成功").data("success").build();
        }
        else {
            return ApiResponse.<String>builder().code(400).message("绑定失败").data("fail").build();
        }
    }

    @ApiOperation(value = "取消卡片和微信用户的绑定", notes = "只传cardId、cardUid、openid三个字段")
    @PutMapping("/unbindUser")
    public ApiResponse<String> unbindUser(@ApiParam(value="",required = true) @RequestBody(required=true) BindUser requestData){

        Integer userId = requestData.getUserId();
        String cardUid = requestData.getCardUid();
        Card entity = cardService.getByCarduid(cardUid);
        if (entity ==null){
            return ApiResponse.<String>builder().code(400).message("取绑失败,输入的uid有误，该卡片不存在").data("fail").build();
        }
        entity.setOpenid("404");
        entity.setIsBind(2);
        entity.setGmtModified(new Date());
        Boolean b = cardService.unbindUser(entity);
        if (b == true){
            return ApiResponse.<String>builder().code(200).message("取绑成功").data(b.toString()).build();
        }
        else {
            return ApiResponse.<String>builder().code(400).message("取绑失败").data(b.toString()).build();
        }
    }


    @ApiOperation(value = "查看卡片余额", notes = "")
    @GetMapping("/getCardMoney")
    public ApiResponse<String> getCardMoney(@ApiParam(value="卡片uid",required = true) @RequestParam(required=true) String cardUid){

        //先判断该卡是否曾经被绑定过，且是否已经解绑
        Card entity = cardService.getByCarduid(cardUid);
        if(entity==null ){
            return ApiResponse.<String>builder().code(400).message("uid输错了").data("fail").build();
        }
        String cardMoney = getAsString(entity.getCardMoney());

        return ApiResponse.<String>builder().code(200).message(cardMoney).data("success").build();

    }

}
