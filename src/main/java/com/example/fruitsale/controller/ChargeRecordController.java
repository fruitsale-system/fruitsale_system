package com.example.fruitsale.controller;

import com.example.fruitsale.common.ApiResponse;
import com.example.fruitsale.entity.Card;
import com.example.fruitsale.entity.ChargeRecord;
import com.example.fruitsale.entity.UserManage;
import com.example.fruitsale.service.CardService;
import com.example.fruitsale.service.ChargeRecordService;
import com.example.fruitsale.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Api(tags = "ChargeRecordController", description = "充值管理")   //tags是api名字，description是简介，value:已废用
@RestController
@RequestMapping("/charge")
public class ChargeRecordController {
    @Autowired
    ChargeRecordService chargeRecordService;
    @Autowired
    CardService cardService;
    @Autowired
    UserService userService;


    @ApiOperation(value = "给卡片充值", notes = "这个接口只有小程序都需要用到。硬件端充值是选择金额后生成二维码，包括cardUid、chargeMoney两个字段，小程序扫码后支付从而生成记录，这种放式chargeWay=2；小程序端线上选择卡片进行充值，直接调用api。二者都需要传入chargeWay、cardUid、userId、chargeMoney、nickName 5个字段")
    @PostMapping("/chargeCard")
    public ApiResponse<String> addCharge(@ApiParam(value="硬件端充值chargeWay填2，小程序充值填1；小程序传参4个，硬件端3个，多余的删去",required = true) @RequestBody(required=true) ChargeRecord requestData){
        ChargeRecord entity = new ChargeRecord();
        //如果是小程序充值，需要多传一个userId
        if(requestData.getChargeWay()==1){
//            entity.setUserId(requestData.getUserId());
//            UserManage userEntity = userService.getById(requestData.getUserId());
//            entity.setNickname(userEntity.getNickname());
            entity.setChargeWay(1);
        }
        else{
            entity.setChargeWay(2);
        }
        entity.setUserId(requestData.getUserId());
        entity.setNickname(requestData.getNickname());
        entity.setChargeMoney(requestData.getChargeMoney());
        entity.setCardUid(requestData.getCardUid());
        entity.setChargeTime(new Date());
        entity.setGmtCreate(new Date());
        //charge表中的充值后金额为原来卡里的金额+本次充值金额，同时要修改card表的卡金额
        Card cardEntity = cardService.getByCarduid(requestData.getCardUid());
        entity.setCardMoney(cardEntity.getCardMoney()+ requestData.getChargeMoney());//现在卡里余额等于原来的加上重的
        cardEntity.setCardMoney(cardEntity.getCardMoney()+ requestData.getChargeMoney());
        cardService.updateCard(cardEntity);

        int b = (chargeRecordService.addChargeRecord(entity));
        if (b == 1){
            return ApiResponse.<String>builder().code(200).message("添加成功").data("success").build();
        }
        else {
            return ApiResponse.<String>builder().code(400).message("添加失败").data("fail").build();
        }
    }

    @ApiOperation(value = "查找我的所有充值记录", notes = "")
    //注意是@PathVariable 不是@PathParam ；这个是路径传值
    @GetMapping("/getMyChargeRecords")
    public ApiResponse<List<ChargeRecord>> getMyChargeRecords(@ApiParam(value="用户id",required = true) @RequestParam(required=true) Integer userId){
        List<ChargeRecord> entities =chargeRecordService.getMyChargeRecords(userId);

        if (entities.size() != 0){
            return ApiResponse.<List<ChargeRecord>>builder().code(200).message("查找成功").data(entities).build();
        }
        else {
            return ApiResponse.<List<ChargeRecord>>builder().code(400).message("查找失败").data(null).build();
        }
    }

    @ApiOperation(value = "查找某张卡的所有充值记录", notes = "")
    //注意是@PathVariable 不是@PathParam ；这个是路径传值
    @GetMapping("/getCardChargeRecords")
    public ApiResponse<List<ChargeRecord>> getCardChargeRecords(@ApiParam(value="卡片uid",required = true) @RequestParam(required=true) String cardUid){
        List<ChargeRecord> entities =chargeRecordService.getCardChargeRecords(cardUid);

        if (entities.size() != 0){
            return ApiResponse.<List<ChargeRecord>>builder().code(200).message("查找成功").data(entities).build();
        }
        else {
            return ApiResponse.<List<ChargeRecord>>builder().code(400).message("查找失败").data(null).build();
        }
    }
}
