package com.example.fruitsale.mapper;

import com.example.fruitsale.entity.UserManage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserManageMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(UserManage record);

    int insertSelective(UserManage record);

    UserManage selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(UserManage record);

    int updateByPrimaryKey(UserManage record);

    UserManage getByOpenid(@Param("openid") String openid);

    @Select("SELECT user_id FROM user_manage ORDER BY user_id DESC")
    int getLastId();

    @Select("SELECT user_id FROM user_manage WHERE openid =#{openid}")
    int getUseridByOpenid(String openid);

    UserManage getUserBySkey(String skey);
}