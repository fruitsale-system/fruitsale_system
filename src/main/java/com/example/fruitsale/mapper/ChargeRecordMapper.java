package com.example.fruitsale.mapper;

import com.example.fruitsale.entity.ChargeRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChargeRecordMapper {
    int deleteByPrimaryKey(Integer chargeId);

    int insert(ChargeRecord record);

    int insertSelective(ChargeRecord record);

    ChargeRecord selectByPrimaryKey(Integer chargeId);

    int updateByPrimaryKeySelective(ChargeRecord record);

    int updateByPrimaryKey(ChargeRecord record);

    List<ChargeRecord> getMyChargeRecords(@Param("userId") Integer userId);

    List<ChargeRecord> getCardChargeRecords(@Param("cardUid") String cardUid);
}