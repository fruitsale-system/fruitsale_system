package com.example.fruitsale.mapper;

import com.example.fruitsale.entity.MsgBoardEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndexMapper {
//    @Select("SELECT * FROM msg_board")
    List<MsgBoardEntity> list();

//    @Insert("INSERT INTO msg_board(author,msg,gmt_create) VALUES(#{author},#{msg},#{gmtCreate})")
    Boolean save(MsgBoardEntity entity);

//    @Delete("DELETE FROM msg_board WHERE id = #{id}“)
    Boolean delete(@Param("id") long id);//一定要记得写@Param，不然xml里面识别不了。
    // 不管是路径传值还是参数传值，都要这么写。多个参数则用逗号隔开。比如Boolean delete(@Param("id") long id,@Param("msg") String msg);

//    @Update("UPDATE msg_board SET msg = #{msg} WHERE id = #{id}")
    Boolean update(@Param("id") long id,@Param("msg") String msg);

    @Select("SELECT * FROM msg_board where author=#{author}")
    List<MsgBoardEntity> getListByauthor(@Param("author") String author);

}
