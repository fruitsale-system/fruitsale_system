package com.example.fruitsale.mapper;

import com.example.fruitsale.entity.Card;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardMapper {
    int deleteByPrimaryKey(Integer cardId);

    int insert(Card record);

    int insertSelective(Card record);

    Card selectByPrimaryKey(Integer cardId);

    int updateByPrimaryKeySelective(Card record);

    int updateByPrimaryKey(Card record);

    List<Card> getCardByOpenid(@Param("openid") String openid);

    Card getCardByUid(@Param("cardUid") String cardUid);

    List<Card> getMyCardsByUserid(@Param("userId") Integer userId);

    @Select("SELECT user_manage.user_id FROM user_manage,card WHERE card.card_uid =#{cardUid} AND card.openid=user_manage.openid")
    Integer getUseridByCarduid(@Param("cardUid") String cardUid);
}