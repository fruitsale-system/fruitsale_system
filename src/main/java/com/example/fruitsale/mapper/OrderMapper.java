package com.example.fruitsale.mapper;

import com.example.fruitsale.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderMapper {
    int deleteByPrimaryKey(Integer orderId);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer orderId);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    List<Order> getOrderByUseridAndBuyway(@Param("userId") Integer userId, @Param("buyWay") Integer buyWay);

    List<Order> getOrderByUseridAndPayway(@Param("userId") Integer userId, @Param("payWay") Integer payWay, @Param("buyWay") Integer buyWay);

    List<Order> getOrderByCarduid(@Param("cardUid") String cardUid);
}