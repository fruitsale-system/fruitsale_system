package com.example.fruitsale.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static com.example.fruitsale.utils.Converter.getAsFloat;

/**
 * 随机数工具类
 *
 * @author wcy
 * 创建时间 14:08 2020/8/27
 */
public class RandomUtils {
    /**
     * 获取指定位数的随机数
     *
     * @param num
     * @return
     */
    public static String getRandom(int num) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < num; i++) {
            sb.append(String.valueOf(random.nextInt(10)));
        }
        return sb.toString();
    }

    /**
     * 获得 区间随机数
     *
     * @param start
     * @param end
     * @return
     */
    public static Integer getRandom(int start, int end) {
        int count = (int) (start + Math.random() * (end - start + 1));
        return count;
    }

    public static String getCouponRandom(int num) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        //创建SimpleDateFormat对象实例并定义好转换格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        System.out.println("把当前时间转换成字符串：" + sdf.format(new Date()));
        String nowTime = sdf.format(new Date());
        for (int i = 0; i < num; i++) {
            sb.append(String.valueOf(random.nextInt(10)));
        }
        return sb.toString() + nowTime;
    }

    public static String getOrderPayNoRandom() {
        //类似4200000929 20210414 4894343674   10+8+10
        Random random = new Random();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        //创建SimpleDateFormat对象实例并定义好转换格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String nowTime = sdf.format(new Date());
        for (int i = 0; i < 3; i++) {
            sb1.append(String.valueOf(random.nextInt(10)));
        }
        for (int i = 0; i < 6; i++) {
            sb2.append(String.valueOf(random.nextInt(10)));
        }
        return "4200000" + sb1.toString() + nowTime + sb2.toString() + "fake";
    }

    public static String getOrderIdRandom() {
        //类似qtt 161837954403 3MAn   3+12+4
        Random random = new Random();
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < 12; i++) {
            sb1.append(String.valueOf(random.nextInt(10)));
        }
        return "qtt" + sb1.toString() + "Fake";
    }

    public static Float getGradeRandom(int a, int b) {
        Float grade = getAsFloat(((Math.random() * (b - a + 1)) + a) / 10);
        return grade;
    }

    public static void main(String[] args) {
        String random = getCouponRandom(6);
        System.out.println(random);
        String payNo = getOrderPayNoRandom();
        System.out.println(payNo);
    }
}