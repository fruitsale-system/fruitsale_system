package com.example.fruitsale.utils;

import com.example.fruitsale.common.ApiResponse;
import com.google.common.collect.Lists;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

/**
 * java1.8 的新特性，解决SimpleDateFormat的线程问题<br>
 * <li>Instant代替 Date，</li>
 * <li>LocalDateTime代替 Calendar，</li>
 * <li>DateTimeFormatter 代替 SimpleDateFormat.</li> 注意：如果是共享变量，则可能会出现线程问题。<br>
 *
 * @author zero 2019/03/30
 */
public class DateUtil {
    // 时间元素
    private static final String YEAR = "year";
    private static final String MONTH = "month";
    private static final String WEEK = "week";
    private static final String DAY = "day";
    private static final String HOUR = "hour";
    private static final String MINUTE = "minute";
    private static final String SECOND = "second";

    // 星期元素
    private static final String MONDAY = "MONDAY";// 星期一
    private static final String TUESDAY = "TUESDAY";// 星期二
    private static final String WEDNESDAY = "WEDNESDAY";// 星期三
    private static final String THURSDAY = "THURSDAY";// 星期四
    private static final String FRIDAY = "FRIDAY";// 星期五
    private static final String SATURDAY = "SATURDAY";// 星期六
    private static final String SUNDAY = "SUNDAY";// 星期日

    // 根据指定格式显示日期和时间
    /**
     * yyyy-MM-dd
     */
    private static final DateTimeFormatter yyyyMMdd_EN = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    /**
     * yyyy-MM-dd HH
     */
    private static final DateTimeFormatter yyyyMMddHH_EN = DateTimeFormatter.ofPattern("yyyy-MM-dd HH");
    /**
     * yyyy-MM-dd HH:mm
     */
    private static final DateTimeFormatter yyyyMMddHHmm_EN = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    /**
     * yyyy-MM-dd HH:mm:ss
     */
    private static final DateTimeFormatter yyyyMMddHHmmss_EN = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    /**
     * HH:mm:ss
     */
    private static final DateTimeFormatter HHmmss_EN = DateTimeFormatter.ofPattern("HH:mm:ss");
    /**
     * yyyy年MM月dd日
     */
    private static final DateTimeFormatter yyyyMMdd_CN = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
    /**
     * yyyy年MM月dd日HH时
     */
    private static final DateTimeFormatter yyyyMMddHH_CN = DateTimeFormatter.ofPattern("yyyy年MM月dd日HH时");
    /**
     * yyyy年MM月dd日HH时mm分
     */
    private static final DateTimeFormatter yyyyMMddHHmm_CN = DateTimeFormatter.ofPattern("yyyy年MM月dd日HH时mm分");
    /**
     * yyyy年MM月dd日HH时mm分ss秒
     */
    private static final DateTimeFormatter yyyyMMddHHmmss_CN = DateTimeFormatter.ofPattern("yyyy年MM月dd日HH时mm分ss秒");
    /**
     * HH时mm分ss秒
     */
    private static final DateTimeFormatter HHmmss_CN = DateTimeFormatter.ofPattern("HH时mm分ss秒");

    // 本地时间显示格式：区分中文和外文显示
    private static final DateTimeFormatter shotDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
    private static final DateTimeFormatter fullDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
    private static final DateTimeFormatter longDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
    private static final DateTimeFormatter mediumDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);

    /**
     * 获取当前日期
     *
     * @return yyyy-MM-dd
     * @author zero 2019/03/30
     */
    public static String getNowDate_EN() {
        return String.valueOf(LocalDate.now());
    }

    /**
     * 获取当前日期
     *
     * @return 字符串yyyy-MM-dd HH:mm:ss
     * @author zero 2019/03/30
     */
    public static String getNowTime_EN() {
        return LocalDateTime.now().format(yyyyMMddHHmmss_EN);
    }

    /**
     * 获取当前时间（yyyy-MM-dd HH）
     */
    public static String getNowTime_EN_yMdH() {
        return LocalDateTime.now().format(yyyyMMddHH_EN);
    }

    /**
     * 获取当前时间（yyyy年MM月dd日）
     */
    public static String getNowTime_CN_yMdH() {
        return LocalDateTime.now().format(yyyyMMddHH_CN);
    }

    /**
     * 获取当前时间（yyyy-MM-dd HH:mm）
     */
    public static String getNowTime_EN_yMdHm() {
        return LocalDateTime.now().format(yyyyMMddHHmm_EN);
    }

    /**
     * 获取当前时间（yyyy年MM月dd日HH时mm分）
     */
    public static String getNowTime_CN_yMdHm() {
        return LocalDateTime.now().format(yyyyMMddHHmm_CN);
    }

    /**
     * 获取当前时间（HH时mm分ss秒）
     */
    public static String getNowTime_CN_HHmmss() {
        return LocalDateTime.now().format(HHmmss_CN);
    }

    /**
     * 根据日期格式，获取当前时间
     *
     * @param formatStr 日期格式<br>
     *                  <li>yyyy</li>
     *                  <li>yyyy-MM-dd</li>
     *                  <li>yyyy-MM-dd HH:mm:ss</li>
     *                  <li>HH:mm:ss</li>
     * @return
     * @author zero 2019/03/30
     */
    public static String getTime(String formatStr) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(formatStr));
    }

    /**
     * 获取中文的当前日期
     *
     * @return yyyy年mm月dd日
     * @author zero 2019/03/30
     */
    public static String getNowDate_CN() {
        return LocalDate.now().format(yyyyMMdd_CN);
    }

    /**
     * 获取中文当前时间
     *
     * @return yyyy年MM月dd日HH时mm分ss秒
     * @author zero 2019/03/30
     */
    public static String getNowTime_CN() {
        return LocalDateTime.now().format(yyyyMMddHHmmss_CN);
    }

    /**
     * 简写本地当前日期：yy-M-dd<br>
     * 例如：19-3-30为2019年3月30日
     *
     * @return 字符串yy-M-dd
     * @author zero 2019/03/30
     */
    public static String getNowLocalTime_shot() {
        return LocalDateTime.now().format(shotDate);
    }

    /**
     * 根据当地日期显示格式：yyyy年M月dd日 星期？（中国）
     *
     * @return 形如：2019年3月30日 星期六
     * @author zero 2019/03/30
     */
    public static String getNowLocalTime_full() {
        return LocalDateTime.now().format(fullDate);
    }

    /**
     * 根据当地显示日期格式：yyyy年M月dd日（中国）
     *
     * @return 形如 2019年3月30日
     * @author zero 2019/03/30
     */
    public static String getNowLocalTime_long() {
        return LocalDateTime.now().format(longDate);
    }

    /**
     * 根据当地显示日期格式：yyyy-M-dd（中国）
     *
     * @return 形如：2019-3-30
     * @author zero 2019/03/30
     */
    public static String getNowLocalTime_medium() {
        return LocalDateTime.now().format(mediumDate);
    }

    /**
     * 获取当前日期的节点时间（年，月，周，日，时，分，秒）
     *
     * @param node 日期中的节点元素（年，月，周，日，时，分，秒）
     * @return 节点数字，如创建此方法的时间：年 2019，月 3，日 30，周 6
     * @author zero 2019/03/30 星期六
     */
    public static Integer getNodeTime(String node) {
        LocalDateTime today = LocalDateTime.now();
        Integer resultNode = null;
        switch (node) {
            case YEAR:
                resultNode = today.getYear();
                break;
            case MONTH:
                resultNode = today.getMonthValue();
                break;
            case WEEK:
                resultNode = transformWeekEN2Num(String.valueOf(today.getDayOfWeek()));
                break;
            case DAY:
                resultNode = today.getDayOfMonth();
                break;
            case HOUR:
                resultNode = today.getHour();
                break;
            case MINUTE:
                resultNode = today.getMinute();
                break;
            case SECOND:
                resultNode = today.getSecond();
                break;
            default:
                // 当前日期是当前年的第几天。例如：2019/1/3是2019年的第三天
                resultNode = today.getDayOfYear();
                break;
        }
        return resultNode;
    }

    /**
     * 将英文星期转换成数字
     *
     * @param enWeek 英文星期
     * @return int，如果数字小于0，则检查，看是否输入错误 or 入参为null
     * @author zero 2019/03/30
     */
    public static int transformWeekEN2Num(String enWeek) {
        if (MONDAY.equals(enWeek)) {
            return 1;
        } else if (TUESDAY.equals(enWeek)) {
            return 2;
        } else if (WEDNESDAY.equals(enWeek)) {
            return 3;
        } else if (THURSDAY.equals(enWeek)) {
            return 4;
        } else if (FRIDAY.equals(enWeek)) {
            return 5;
        } else if (SATURDAY.equals(enWeek)) {
            return 6;
        } else if (SUNDAY.equals(enWeek)) {
            return 7;
        } else {
            return -1;
        }
    }

    /**
     * 获取当前日期之后（之后）的节点事件<br>
     * <ul>
     * 比如当前时间为：2019-03-30 10:20:30
     * </ul>
     * <li>node="hour",num=5L:2019-03-30 15:20:30</li>
     * <li>node="day",num=1L:2019-03-31 10:20:30</li>
     * <li>node="year",num=1L:2020-03-30 10:20:30</li>
     *
     * @param node 节点元素（“year”,"month","week","day","huor","minute","second"）
     * @param num  第几天（+：之后，-：之前）
     * @return 之后或之后的日期
     * @author zero 2019/03/30
     */
    public static String getAfterOrPreNowTime(String node, Long num) {
        LocalDateTime now = LocalDateTime.now();
        if (HOUR.equals(node)) {
            return now.plusHours(num).format(yyyyMMddHHmmss_EN);
        } else if (DAY.equals(node)) {
            return now.plusDays(num).format(yyyyMMddHHmmss_EN);
        } else if (WEEK.equals(node)) {
            return now.plusWeeks(num).format(yyyyMMddHHmmss_EN);
        } else if (MONTH.equals(node)) {
            return now.plusMonths(num).format(yyyyMMddHHmmss_EN);
        } else if (YEAR.equals(node)) {
            return now.plusYears(num).format(yyyyMMddHHmmss_EN);
        } else if (MINUTE.equals(node)) {
            return now.plusMinutes(num).format(yyyyMMddHHmmss_EN);
        } else if (SECOND.equals(node)) {
            return now.plusSeconds(num).format(yyyyMMddHHmmss_EN);
        } else {
            return "Node is Error!";
        }
    }

    /**
     * 获取与当前日期相距num个之后（之前）的日期<br>
     * <ul>
     * 比如当前时间为：2019-03-30 10:20:30的格式日期
     * <li>node="hour",num=5L:2019-03-30 15:20:30</li>
     * <li>node="day",num=1L:2019-03-31 10:20:30</li>
     * <li>node="year",num=1L:2020-03-30 10:20:30</li>
     * </ul>
     *
     * @param dtf  格式化当前时间格式（dtf = yyyyMMddHHmmss_EN）
     * @param node 节点元素（“year”,"month","week","day","huor","minute","second"）
     * @param num  （+：之后，-：之前）
     * @return 之后之前的日期
     * @author zero 2019/03/30
     */
    public static String getAfterOrPreNowTimePlus(DateTimeFormatter dtf, String node, Long num) {
        LocalDateTime now = LocalDateTime.now();
        if (HOUR.equals(node)) {
            return now.plusHours(num).format(dtf);
        } else if (DAY.equals(node)) {
            return now.plusDays(num).format(dtf);
        } else if (WEEK.equals(node)) {
            return now.plusWeeks(num).format(dtf);
        } else if (MONTH.equals(node)) {
            return now.plusMonths(num).format(dtf);
        } else if (YEAR.equals(node)) {
            return now.plusYears(num).format(dtf);
        } else if (MINUTE.equals(node)) {
            return now.plusMinutes(num).format(dtf);
        } else if (SECOND.equals(node)) {
            return now.plusSeconds(num).format(dtf);
        } else {
            return "Node is Error!";
        }
    }

    /**
     * 当前时间的hour，minute，second之后（之前）的时刻
     *
     * @param node 时间节点元素（hour，minute，second）
     * @param num  之后（之后）多久时，分，秒（+：之后，-：之前）
     * @return HH:mm:ss 字符串
     * @author zero 2019/03/30
     */
    public static String getAfterOrPreNowTimeSimp(String node, Long num) {
        LocalTime now = LocalTime.now();
        if (HOUR.equals(node)) {
            return now.plusHours(num).format(HHmmss_EN);
        } else if (MINUTE.equals(node)) {
            return now.plusMinutes(num).format(HHmmss_EN);
        } else if (SECOND.equals(node)) {
            return now.plusSeconds(num).format(HHmmss_EN);
        } else {
            return "Node is Error!";
        }
    }

    /**
     * 检查重复事件，比如生日。TODO This is a example.
     * <p>
     * //     * @param birthDayStr
     *
     * @return
     * @author zero 2019/03/31
     */
    public static boolean isBirthday(int month, int dayOfMonth) {
        MonthDay birthDay = MonthDay.of(month, dayOfMonth);
        MonthDay curMonthDay = MonthDay.from(LocalDate.now());// MonthDay只存储了月、日。
        if (birthDay.equals(curMonthDay)) {
            return true;
        }
        return false;
    }

    /**
     * 获取当前日期第index日之后(之前)的日期（yyyy-MM-dd）
     *
     * @param index 第index天
     * @return 日期字符串：yyyy-MM-dd
     * @author zero 2019/03/31
     */
    public static String getAfterOrPreDayDate(int index) {
        return LocalDate.now().plus(index, ChronoUnit.DAYS).format(yyyyMMdd_EN);
    }

    /**
     * 获取当前日期第index周之前（之后）的日期（yyyy-MM-dd）
     *
     * @param index 第index周（+：之后，-：之前）
     * @return 日期字符串：yyyy-MM-dd
     * @author zero 2019/03/31
     */
    public static String getAfterOrPreWeekDate(int index) {
        return LocalDate.now().plus(index, ChronoUnit.WEEKS).format(yyyyMMdd_EN);
    }

    /**
     * 获取当前日期第index月之前（之后）的日期（yyyy-MM-dd）
     *
     * @param index 第index月（+：之后，-：之前）
     * @return 日期字符串：yyyy-MM-dd
     * @author zero 2019/03/31
     */
    public static String getAfterOrPreMonthDate(int index) {
        return LocalDate.now().plus(index, ChronoUnit.MONTHS).format(yyyyMMdd_EN);
    }

    /**
     * 获取当前日期第index年之前（之后）的日期（yyyy-MM-dd）
     *
     * @param index 第index年（+：之后，-：之前）
     * @return 日期字符串：yyyy-MM-dd
     * @author zero 2019/03/31
     */
    public static String getAfterOrPreYearDate(int index) {
        return LocalDate.now().plus(index, ChronoUnit.YEARS).format(yyyyMMdd_EN);
    }

    /**
     * 获取指定日期之前之后的第index的日，周，月，年的日期
     *
     * @param date  指定日期格式：yyyy-MM-dd
     * @param node  时间节点元素（日周月年）
     * @param index 之前之后第index个日期
     * @return yyyy-MM-dd 日期字符串
     * @author zero 2019/03/31
     */
    public static String getAfterOrPreDate(String date, String node, int index) {
        date = date.trim();
        if (DAY.equals(node)) {
            return LocalDate.parse(date).plus(index, ChronoUnit.DAYS).format(yyyyMMdd_EN);
        } else if (WEEK.equals(node)) {
            return LocalDate.parse(date).plus(index, ChronoUnit.WEEKS).format(yyyyMMdd_EN);
        } else if (MONTH.equals(node)) {
            return LocalDate.parse(date).plus(index, ChronoUnit.MONTHS).format(yyyyMMdd_EN);
        } else if (YEAR.equals(node)) {
            return LocalDate.parse(date).plus(index, ChronoUnit.YEARS).format(yyyyMMdd_EN);
        } else {
            return "Wrong date format!";
        }
    }

    /**
     * 检测：输入年份是否是闰年？
     *
     * @param date 日期格式：yyyy-MM-dd
     * @return true：闰年，false：平年
     * @author zero 2019/03/31
     */
    public static boolean isLeapYear(String date) {
        return LocalDate.parse(date.trim()).isLeapYear();
    }

    /**
     * 计算两个日期字符串之间相差多少个周期（天，月，年）
     *
     * @param date1 yyyy-MM-dd
     * @param date2 yyyy-MM-dd
     * @param node  三者之一:(day，month,year)
     * @return 相差多少周期
     * @author zero 2019/03/31
     */
    public static int peridCount(String date1, String date2, String node) {
        date1 = date1.trim();
        date2 = date2.trim();
        if (DAY.equals(node)) {
            return Period.between(LocalDate.parse(date1), LocalDate.parse(date2)).getDays();
        } else if (MONTH.equals(node)) {
            return Period.between(LocalDate.parse(date1), LocalDate.parse(date2)).getMonths();
        } else if (YEAR.equals(node)) {
            return Period.between(LocalDate.parse(date1), LocalDate.parse(date2)).getYears();
        } else {
            return 0;
        }
    }

    /**
     * 切割日期。按照周期切割成小段日期段。例如： <br>
     *
     * @param startDate 开始日期（yyyy-MM-dd）
     * @param endDate   结束日期（yyyy-MM-dd）
     * @param period    周期（天，周，月，年）
     * @return 切割之后的日期集合
     * @author zero 2019/04/02
     * @example <li>startDate="2019-02-28",endDate="2019-03-05",period="day"</li>
     * <li>结果为：[2019-02-28, 2019-03-01, 2019-03-02, 2019-03-03, 2019-03-04, 2019-03-05]</li><br>
     * <li>startDate="2019-02-28",endDate="2019-03-25",period="week"</li>
     * <li>结果为：[2019-02-28,2019-03-06, 2019-03-07,2019-03-13, 2019-03-14,2019-03-20,
     * 2019-03-21,2019-03-25]</li><br>
     * <li>startDate="2019-02-28",endDate="2019-05-25",period="month"</li>
     * <li>结果为：[2019-02-28,2019-02-28, 2019-03-01,2019-03-31, 2019-04-01,2019-04-30,
     * 2019-05-01,2019-05-25]</li><br>
     * <li>startDate="2019-02-28",endDate="2020-05-25",period="year"</li>
     * <li>结果为：[2019-02-28,2019-12-31, 2020-01-01,2020-05-25]</li><br>
     */
    public static List<String> getPieDateRange(String startDate, String endDate, String period) {
        List<String> result = Lists.newArrayList();
        LocalDate end = LocalDate.parse(endDate, yyyyMMdd_EN);
        LocalDate start = LocalDate.parse(startDate, yyyyMMdd_EN);
        LocalDate tmp = start;
        switch (period) {
            case DAY:
                while (start.isBefore(end) || start.isEqual(end)) {
                    result.add(start.toString());
                    start = start.plusDays(1);
                }
                break;
            case WEEK:
                while (tmp.isBefore(end) || tmp.isEqual(end)) {
                    if (tmp.plusDays(6).isAfter(end)) {
                        result.add(tmp.toString() + "," + end);
                    } else {
                        result.add(tmp.toString() + "," + tmp.plusDays(6));
                    }
                    tmp = tmp.plusDays(7);
                }
                break;
            case MONTH:
                while (tmp.isBefore(end) || tmp.isEqual(end)) {
                    LocalDate lastDayOfMonth = tmp.with(TemporalAdjusters.lastDayOfMonth());
                    if (lastDayOfMonth.isAfter(end)) {
                        result.add(tmp.toString() + "," + end);
                    } else {
                        result.add(tmp.toString() + "," + lastDayOfMonth);
                    }
                    tmp = lastDayOfMonth.plusDays(1);
                }
                break;
            case YEAR:
                while (tmp.isBefore(end) || tmp.isEqual(end)) {
                    LocalDate lastDayOfYear = tmp.with(TemporalAdjusters.lastDayOfYear());
                    if (lastDayOfYear.isAfter(end)) {
                        result.add(tmp.toString() + "," + end);
                    } else {
                        result.add(tmp.toString() + "," + lastDayOfYear);
                    }
                    tmp = lastDayOfYear.plusDays(1);
                }
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * 指定日期月的最后一天（yyyy-MM-dd）
     *
     * @param curDate     日期格式（yyyy-MM-dd）
     * @param firstOrLast true：第一天，false：最后一天
     * @return
     * @author zero 2019/04/13
     */
    public static String getLastDayOfMonth(String curDate, boolean firstOrLast) {
        if (firstOrLast) {
            return LocalDate.parse(curDate, yyyyMMdd_EN).with(TemporalAdjusters.firstDayOfMonth()).toString();
        } else {
            return LocalDate.parse(curDate, yyyyMMdd_EN).with(TemporalAdjusters.lastDayOfMonth()).toString();
        }
    }

    /**
     * 指定日期年的最后一天（yyyy-MM-dd）
     *
     * @param curDate     指定日期（格式：yyyy-MM-dd）
     * @param firstOrLast true:第一天，false:最后一天
     * @return
     * @author zero 2019/04/13
     */
    public static String getLastDayOfYear(String curDate, boolean firstOrLast) {
        if (firstOrLast) {
            return LocalDate.parse(curDate, yyyyMMdd_EN).with(TemporalAdjusters.firstDayOfYear()).toString();
        } else {
            return LocalDate.parse(curDate, yyyyMMdd_EN).with(TemporalAdjusters.lastDayOfYear()).toString();
        }
    }

    /**
     * 获取下一个星期的日期
     *
     * @param curDay          yyyy-MM-dd
     * @param dayOfWeek       monday:1~sunday:7
     * @param isContainCurDay 是否包含当天，true：是，false：不包含
     * @return 日期（yyyy-MM-dd）
     * @author zero 2019/04/02
     */
    public static String getNextWeekDate(String curDay, int dayOfWeek, boolean isContainCurDay) {
        dayOfWeek = dayOfWeek < 1 || dayOfWeek > 7 ? 1 : dayOfWeek;
        if (isContainCurDay) {
            return LocalDate.parse(curDay).with(TemporalAdjusters.nextOrSame(DayOfWeek.of(dayOfWeek))).toString();
        } else {
            return LocalDate.parse(curDay).with(TemporalAdjusters.next(DayOfWeek.of(dayOfWeek))).toString();
        }
    }

    /**
     * 获取上一个星期的日期
     *
     * @param curDay    指定日期（yyyy-MM-dd）
     * @param dayOfWeek 数字范围（monday:1~sunday:7）
     * @param isCurDay  是否包含当天，true：是，false：不包含
     * @return 日期（yyyy-MM-dd）
     * @author zero 2019/04/02
     */
    public static String getPreWeekDate(String curDay, int dayOfWeek, boolean isCurDay) {
        dayOfWeek = dayOfWeek < 1 || dayOfWeek > 7 ? 1 : dayOfWeek;
        if (isCurDay) {
            return LocalDate.parse(curDay).with(TemporalAdjusters.previousOrSame(DayOfWeek.of(dayOfWeek))).toString();
        } else {
            return LocalDate.parse(curDay).with(TemporalAdjusters.previous(DayOfWeek.of(dayOfWeek))).toString();
        }
    }

    /**
     * 获取指定日期当月的最后或第一个星期日期
     *
     * @param curDay      指定日期（yyyy-MM-dd）
     * @param dayOfWeek   周几（1~7）
     * @param lastOrFirst true：最后一个，false本月第一个
     * @return 日期（yyyy-MM-dd）
     * @author zero 2019/04/02
     */
    public static String getFirstOrLastWeekDate(String curDay, int dayOfWeek, boolean lastOrFirst) {
        dayOfWeek = dayOfWeek < 1 || dayOfWeek > 7 ? 1 : dayOfWeek;
        if (lastOrFirst) {
            return LocalDate.parse(curDay).with(TemporalAdjusters.lastInMonth(DayOfWeek.of(dayOfWeek))).toString();
        } else {
            return LocalDate.parse(curDay).with(TemporalAdjusters.firstInMonth(DayOfWeek.of(dayOfWeek))).toString();
        }
    }
//    以上都是原来的工具类

    //传入的时间字符串按照所给的格式转换成Date
    public static Date toDate(String dateStr, String formaterString) {
        Date date = null;
        SimpleDateFormat formater = new SimpleDateFormat();
        formater.applyPattern(formaterString);
        try {
            date = formater.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    //传入的Date时间以想要的格式转换成字符串
    public static String tostring(Date date, String formaterString) {
        String time;
        SimpleDateFormat formater = new SimpleDateFormat();
        formater.applyPattern(formaterString);
        time = formater.format(date);
        return time;
    }


    //补充的方法，比较两个时间哪个早，需要写throws ParseException。 a早于b返回true，否则返回false
    public static boolean compare(String time1, String time2) {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //将字符串形式的时间转化为Date类型的时间
        Date a = null;
        Date b = null;
        try {
            a = sdf.parse(time1);
            b = sdf.parse(time2);//抛出异常
        } catch (ParseException e) {
            e.printStackTrace();
        }

//　　　　　//方法一：
        //Date类的一个方法，如果a早于b返回true，否则返回false
        if (a.before(b))
            return true;
        else
            return false;

//　　　　　//方法二：
        /*
         * 如果你不喜欢用上面这个太流氓的方法，也可以根据将Date转换成毫秒
        if(a.getTime()-b.getTime()<0)
            return true;
        else
            return false;
        */
    }

    //改写。判断一个日期是否在一个时间段内。在的话，则返回true，否则返回false
//   public static boolean isInTimeRange(String time1,String startTime,String endTime) throws ParseException {
    public static boolean isInTimeRange(String time1, String startTime, String endTime) {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //将字符串形式的时间转化为Date类型的时间
        Date myTime = null;
        Date start = null;
        Date end = null;
        try {
            myTime = sdf.parse(time1);
            start = sdf.parse(startTime);
            end = sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        方法一：
//        if(myTime.before(start)==false && myTime.before(end)==true){return true;}
//        else{return false;}'
        //晚于开始时间，且早于结束时间，说明在这个范围内，返回true
        if (myTime.getTime() - start.getTime() >= 0 && myTime.getTime() - end.getTime() <= 0) {
            return true;
        } else {
            return false;
        }
    }

    //改写。判断一个日期是否在一个时间段内。在这之前返回1，在这中间返回2，在这后面返回3
    public static Integer isInTimeRange2(String time1, String startTime, String endTime) {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//注意这里HH表示24小时，hh表示12小时
        //将字符串形式的时间转化为Date类型的时间
        Date myTime = null;
        Date start = null;
        Date end = null;
        try {
            myTime = sdf.parse(time1);
            start = sdf.parse(startTime);
            end = sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //        方法一：
//        if(myTime.before(start)==false && myTime.before(end)==true){return true;}
//        else{return false;}'
//        System.out.println("函数里面mytime="+myTime.getTime()+";start="+start.getTime()+";end="+end.getTime());
//        if(myTime.getTime()-start.getTime()>=0 && myTime.getTime()-end.getTime()<=0){
        if (myTime.before(start) == false && myTime.before(end)) {
            return 2;
        } else if (myTime.before(start)) {
            return 1;
        } else {
            return 3;
        }
    }

    //依照course_class表的duration，得到一个开始~结束时间的区间字符串
    public static String getCourseTimeString(String duration) {
        String[] durationList = duration.split(",");
        String courseTime = "";
        if (durationList.length > 1) {
            //获取开始日期
            String[] startList = durationList[0].split(" ");
            String startTime = startList[0];
            System.out.println("startTime=" + startTime);
            //获取结束日期
            String[] endList = durationList[durationList.length - 1].split(" ");
            String endTime = endList[0];
            System.out.println("endTime=" + endTime);
            //整合
            courseTime = startTime + "~" + endTime;

            String startTimeDate = startTime.replace('年', '-').replace('月', '-').replace('日', ' ');
            String endTimeDate = endTime.replace('年', '-').replace('月', '-').replace('日', ' ');
//            Date endTimeDate = toDate(endTime,"yyyy-MM-dd HH:mm:ss");
            System.out.println("startDate=" + startTimeDate + "; endDate=" + endTimeDate);
            String nowTime = getNowTime_EN();
            Integer timeFlag = isInTimeRange2(nowTime, startTimeDate + "00:00:00", endTimeDate + "00:00:00");
            System.out.println("timeFlag=" + timeFlag);
        }
        if (durationList.length == 1) {
            courseTime = durationList[0];
            String[] startList = durationList[0].split(" ");
            String startTime = startList[0];
            String[] startHHMMList = startList[1].split("-");
            String time1 = startHHMMList[0] + ":00";
            String time2 = startHHMMList[1] + ":00";
            String startTimeDate2 = startTime.replace('年', '-').replace('月', '-').replace('日', ' ') + time1;
            System.out.println("time1=" + time1 + ";time2=" + time2);
            String endTimeDate2 = startTime.replace('年', '-').replace('月', '-').replace('日', ' ') + time2;
            System.out.println("只有一个时间段时，课程时间显示为：" + courseTime);
            String nowTime = getNowTime_EN();
            Integer timeFlag2 = isInTimeRange2(nowTime, startTimeDate2, endTimeDate2);
            System.out.println("nowTime=" + nowTime + ";startTime=" + startTimeDate2 + ";endTime=" + endTimeDate2 + ";timeFlag=" + timeFlag2);
        }

        return courseTime;

    }

    //判断当前时间是否在course_class表的duration时间区间内，还没到返回1，在这区间返回2，晚于这个时间返回3
    public static Map<String, Object> isNowTimeInDuration(String duration) {
        Map<String, Object> map = new HashMap<>();
        String courseTime = "";
        Integer timeFlag = 0;
        String[] durationList = duration.split(",");
        if (durationList.length > 1) {
            //获取开始日期
            String[] startList = durationList[0].split(" ");
            String startTime = startList[0];
            System.out.println("startTime=" + startTime);
            //获取结束日期
            String[] endList = durationList[durationList.length - 1].split(" ");
            String endTime = endList[0];
            System.out.println("endTime=" + endTime);
            //整合
            courseTime = startTime + "~" + endTime;

            String startTimeDate = startTime.replace('年', '-').replace('月', '-').replace('日', ' ');
            String endTimeDate = endTime.replace('年', '-').replace('月', '-').replace('日', ' ');
            System.out.println("startDate=" + startTimeDate + "; endDate=" + endTimeDate);
            String nowTime = getNowTime_EN();
            timeFlag = isInTimeRange2(nowTime, startTimeDate + "00:00:00", endTimeDate + "00:00:00");
            System.out.println("timeFlag=" + timeFlag);
        }
        if (durationList.length == 1) {
            //如果只有一个时间点，那么直接返回今天的日期和时间区间
            courseTime = durationList[0];
            //获取第一个时间的后半段部分，日期和时间以空格分隔
            String[] startList = courseTime.split(" ");
            String startTime = startList[0];
            //后段部分为时间区间，且以-分隔
            String[] startHHMMList = startList[1].split("-");
            String time1 = startHHMMList[0] + ":00";
            String time2 = startHHMMList[1] + ":00";
            //将日期和时间拼接，并取代中文的年月日
            String startTimeDate = startTime.replace('年', '-').replace('月', '-').replace('日', ' ') + time1;
            System.out.println("time1=" + time1 + ";time2=" + time2);
            String endTimeDate = startTime.replace('年', '-').replace('月', '-').replace('日', ' ') + time2;
            //调用函数判断现在时间和给定duration的关系
            System.out.println("只有一个时间段时，课程时间显示为：" + courseTime);
            String nowTime = getNowTime_EN();
            timeFlag = isInTimeRange2(nowTime, startTimeDate, endTimeDate);
            System.out.println("nowTime=" + nowTime + ";startTime=" + startTimeDate + ";endTime=" + endTimeDate + ";timeFlag=" + timeFlag);
        }

        //返回map
        map.put("courseTime", courseTime);
        map.put("timeFlag", timeFlag);
        return map;

    }

    //判断当前课时数，以course_class表的duration作为区间.返回当前学到第几课时
    public static Integer getOfflineCourseAlreadyStudyNum(String duration) {
        Map<String, Object> map = new HashMap<>();
        String courseTime = "";
        Integer timeFlag = 0;
        String[] durationList = duration.split(",");
        //遍历，将duration截取为新的时间段
        List<String> newClassTimeList = new ArrayList<>();
        if (durationList.length > 1) {
            for (int i = 0; i <= durationList.length; i++) {
                if (i < durationList.length) {
                    String datetime = durationList[i];
                    String[] dateOrtime = datetime.split(" ");
                    String datetimeFormat = dateOrtime[0].replace('年', '-').replace('月', '-').replace('日', ' ') + "00:00:00";
                    newClassTimeList.add(datetimeFormat);
                }
                //把最后一个时间点也加进去
                if (i == durationList.length) {
                    String lastDatetime = durationList[durationList.length - 1];
                    String[] lastDateOrtime = lastDatetime.split(" ");
                    String[] timeHHMMList = lastDateOrtime[1].split("-");
                    String time2 = timeHHMMList[1] + ":00";
                    String lastDatetimeFormat = lastDateOrtime[0].replace('年', '-').replace('月', '-').replace('日', ' ') + time2;
                    newClassTimeList.add(lastDatetimeFormat);
                }
            }

//            System.out.println("新的日期列表为"+newClassTimeList);
            for (int i = 0; i < durationList.length; i++) {
                String nowTime = getNowTime_EN();
                Integer flag = isInTimeRange2(nowTime, newClassTimeList.get(i), newClassTimeList.get(i + 1));
                //判断一个日期是否在一个时间段内。在这之前返回1，在这中间返回2，在这后面返回3
                if (flag == 2) {
                    return i + 1;
                }
                if (flag == 1) {
                    return 0;
                }
            }
        }
        if (durationList.length == 1) {
            //如果只有一个时间点，那么直接返回今天的日期和时间区间
            courseTime = durationList[0];
            //获取第一个时间的后半段部分，日期和时间以空格分隔
            String[] startList = courseTime.split(" ");
            String startTime = startList[0];
            //后段部分为时间区间，且以-分隔
            String[] startHHMMList = startList[1].split("-");
            String time1 = startHHMMList[0] + ":00";
            String time2 = startHHMMList[1] + ":00";
            //将日期和时间拼接，并取代中文的年月日
            String startTimeDate = startTime.replace('年', '-').replace('月', '-').replace('日', ' ') + time1;

            String endTimeDate = startTime.replace('年', '-').replace('月', '-').replace('日', ' ') + time2;
            //调用函数判断现在时间和给定duration的关系

            //在这之前返回1，在这中间返回2，在这后面返回3
            String nowTime = getNowTime_EN();
            timeFlag = isInTimeRange2(nowTime, startTimeDate, endTimeDate);
            if (timeFlag == 2 || timeFlag == 3) {
                return 1;
            }
            if (timeFlag == 1) {
                return 0;
            }
        }
        return 0;

    }

    //如果是要用我自己补充的比较日期，需要加上throws Exception
    public static void main(String[] args) {

        String d1str= "2021-06-21 12:20:29";
        Date d1 = toDate(d1str,"yyyy-MM-dd HH:mm:ss");
//        Date d1 = new Date(2021, 6, 21, 19, 00,  00);
        System.out.println("d1="+d1+",d2="+new Date());
        System.out.println("d1="+d1.getTime()+",d2="+new Date().getTime());
        if(new Date().getTime()-d1.getTime()>7*24*60*60*1000){
            System.out.println("超过七天了，d1="+d1.getTime()+",d2="+new Date().getTime());
//            return ApiResponse.<Map<String, Object>>builder().code(400).message("skey过期了，需重新登录").data(result).build();
        }

        String durationStr = new String("2021年3月25日 8:00-12:00,2021年4月2日 8:00-12:00,2021年4月5日 8:00-12:00,2021年4月29日 8:00-23:00");
//        String durationStr = new String("2021年4月21日 7:00-14:00");
        Integer dijicike = getOfflineCourseAlreadyStudyNum(durationStr);
        System.out.println("第几次课了：" + dijicike);
//        Map<String,Object> map = isNowTimeInDuration(durationStr);
//        System.out.println("map="+map);

//        //course_class表的duration是否在日期内
////        String durantionStr = new String("2021年3月25日 8:00-12:00,2021年4月2日 8:00-12:00,2021年4月5日 8:00-12:00");
//        String durantionStr = new String("2021年4月21日 7:00-14:00");
//        String[] durationList = durantionStr.split(",");
////        System.out.println(durationList.length);
//        if(durationList.length>1){
//            //获取开始日期
//            String[] startList = durationList[0].split(" ");
//            String startTime = startList[0];
//            System.out.println("startTime="+startTime);
//            //获取结束日期
//            String[] endList = durationList[durationList.length-1].split(" ");
//            String endTime = endList[0];
//            System.out.println("endTime="+endTime);
//            //整合
//            String courseTime =startTime+"~"+endTime;
//
//            String startTimeDate = startTime.replace('年','-').replace('月','-').replace('日',' ');
//            String endTimeDate = endTime.replace('年','-').replace('月','-').replace('日',' ');
//            System.out.println("startDate="+startTimeDate+"; endDate="+endTimeDate);
//            String nowTime = getNowTime_EN();
//            Integer timeFlag = isInTimeRange2(nowTime,startTimeDate+"00:00:00",endTimeDate+"00:00:00");
//            System.out.println("timeFlag="+timeFlag);
//        }
//        if(durationList.length == 1){
//            String courseTime = durationList[0];
//            String[] startList = durationList[0].split(" ");
//            String startTime = startList[0];
//            String[] startHHMMList = startList[1].split("-");
//            String time1 = startHHMMList[0]+":00";
//            String time2 = startHHMMList[1]+":00";
//            String startTimeDate2 = startTime.replace('年','-').replace('月','-').replace('日',' ')+time1;
//            System.out.println("time1="+time1+";time2="+time2);
//            String endTimeDate2 = startTime.replace('年','-').replace('月','-').replace('日',' ')+time2;
//            System.out.println("只有一个时间段时，课程时间显示为："+courseTime);
//            String nowTime = getNowTime_EN();
//            Integer timeFlag2 = isInTimeRange2(nowTime,startTimeDate2,endTimeDate2);
//            System.out.println("nowTime="+nowTime+";startTime="+startTimeDate2+";endTime="+endTimeDate2+";timeFlag="+timeFlag2);
//        }


//        //1，判断优惠券是否在有效期内
//        System.out.println("现在时间是："+getNowTime_EN());
//        String nowTime = getNowTime_EN();
//        String timeRange = new String("2021-04-06~2021-12-21");
//        String[] timeRange2= timeRange.split("~");
//        System.out.println("优惠券开始时间是："+timeRange2[0]+";过期时间是："+timeRange2[1]);//选择第2个元素
//        //1.2比较两个日期
//        boolean result=new DateUtil().isInTimeRange(nowTime,timeRange2[0]+" 00:00:00",timeRange2[1]+" 00:00:00");//a早于b返回true，否则返回false
//        System.out.println("是否在优惠券有效期内："+result);
//        System.out.println("===================");

        // String curDate = "2019-04-10"; // 指定日期
        // System.out.println(getLastDayOfMonth(curDate, true));
        // System.out.println(getLastDayOfMonth(curDate, false));
        // System.out.println(getLastDayOfYear(curDate, true));
        // System.out.println(getLastDayOfYear(curDate, false));
        // System.out.println("===================");
        // String startDate = "2019-02-28", endDate = "2019-03-05", period = DAY;
        // System.out.println(getPieDateRange(startDate, endDate, period));
        // System.out.println("===================");
        // System.out.println(getNextWeekDate("2019-02-28", 1, false));
        // System.out.println(getPieDateRange("2019-12-28", "2020-03-01", DAY));
        // System.out.println("===================");
        // System.out.println(getFirstOrLastWeekDate("2019-04-02", 0, false));
        // System.out.println(getPreWeekDate("2019-04-02", 2, false));
        // System.out.println(getNextWeekDate("2019-04-02", 2, false));
        // System.out.println("===================");
        // System.out.println("当前时间戳：" + Instant.now());
        // System.out.println("当前时间：" + LocalDateTime.now());
        // System.out.println("===================");
        // System.out.println(peridCount("2019-01-30", "2019-03-31", MONTH));
        // System.out.println(isLeapYear("2019-03-31"));
        // System.out.println(LocalDate.now().isLeapYear());
        // System.out.println("===================");
        // System.out.println(getAfterOrPreDate("2019-03-31", WEEK, -1));
        // System.out.println(getAfterOrPreDayDate(-5));
        // System.out.println(getAfterOrPreDayDate(-3));
        // System.out.println(getAfterOrPreDayDate(6));
        // System.out.println(getAfterOrPreYearDate(6));
        // System.out.println(getAfterOrPreWeekDate(1));
        // System.out.println("===================");
        // clock();
        // isBirthday(7, 13);
        // LocalDate date0 = LocalDate.of(2019, Month.OCTOBER, 31);
        // LocalDate date = LocalDate.of(2019, 3, 31);
        // System.out.println(date0.equals(LocalDate.now()));
        // System.out.println(date.getYear() + "-" + date.getMonthValue() + "-" + date.getDayOfMonth());
        // System.out.println(getNowTime_EN());
        // System.out.println(getAfterNowTimeSimp(SECOND, 5L));
        // System.out.println("===================");
        // System.out.println(getAfterNowTime(SECOND, 5L));
        // System.out.println(getAfterNowTimePlus(yyyyMMddHHmm_EN, SECOND, 5L));
        // System.out.println("===================");
        // System.out.println(getNowDate_EN());
        // System.out.println(getNowDate_CN());
        // System.out.println(getTime("HH:mm:ss"));
        // System.out.println(getNowTime_EN());
        // System.out.println(getNowTime_CN());
        // System.out.println(getNowLocalTime_shot());
        // System.out.println(getNowLocalTime_full());
        // System.out.println(getNowLocalTime_long());
        // System.out.println(getNowLocalTime_medium());
        // System.out.println(getNodeTime("week"));
        // System.out.println(transformWeekEN2Num(null));
        // System.out.println("===================");
    }

}

