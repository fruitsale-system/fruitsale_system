package com.example.fruitsale.utils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import java.util.*;

import static com.example.fruitsale.utils.JSONUtils.toJSONString;
import static com.example.fruitsale.utils.JsonUtils3.stringToList;

/***
 * 用jackson包实现json、对象、Map之间的转换
 * @author Timcoding
 *
 */
public class JSONUtils2 {

    /**
     * 将 Array,list,set 解析成 Json 串
     *
     * @return Json 串
     */
    public static String arrayToJsonStr(Object objs) {
        JSONArray json = JSONArray.fromObject(objs);
        return json.toString();
    }

    /***
     * 将javabean对象和map对象 解析成 Json 串
     * @param obj
     * @return
     */
    public static String objectToJsonStr(Object obj) {
        JSONObject json = JSONObject.fromObject(obj);
        return json.toString();

    }

    /***
     * 将javabean对象或者map对象 解析成 Json 串,使用JsonConfig 过滤属性
     * @param obj
     * @param config
     * @return
     */
    public static String objectToJsonStr(Object obj, JsonConfig config) {
//		JsonConfig config = new JsonConfig();
//	    config.setExcludes(new String[] { "name" });
        JSONObject json = JSONObject.fromObject(obj, config);
        return json.toString();

    }

    /**
     * 将  Json 串 解析成 Array,list,set
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> Collection<T> jsonStrToArray(String jsonStr) {
        JSONArray jsonArray = JSONArray.fromObject(jsonStr);
        Object array = JSONArray.toArray(jsonArray);
        return (Collection<T>) array;
    }

    /**
     * 将  Json 串 解析成 Array
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] jsonStrToArray(String jsonStr, Class<T> clazz) {
        JSONArray jsonArray = JSONArray.fromObject(jsonStr);
        return (T[]) JSONArray.toArray(jsonArray, clazz);
    }

    /**
     * 将  Json 串 解析成 Collection
     *
     * @return
     */
    public static <T> Collection<T> jsonStrToCollection(String jsonStr, Class<T> clazz) {
        JSONArray jsonArray = JSONArray.fromObject(jsonStr);
        @SuppressWarnings("unchecked")
        Collection<T> array = JSONArray.toCollection(jsonArray, clazz);
        return array;
    }

    /**
     * 将  Json 串 解析成 list
     *
     * @return
     */
    public static <T> List<T> jsonStrToList(String jsonStr, Class<T> clazz) {
        return (List<T>) jsonStrToCollection(jsonStr, clazz);
    }

    /**
     * 将  Json 串 解析成 Map或者javabean
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T jsonStrToObject(String jsonStr, Class<T> clazz) {
        JSONObject json = JSONObject.fromObject(jsonStr);
        Object obj = JSONObject.toBean(json, clazz);
        return (T) obj;
    }


    public static void main(String[] args) {

//        //1，创建返回体列表
//        List<Video> videoList = new ArrayList<>();
//        //2，创建返回体的单个实体
//        Video videoEntity0 = new Video();
//        Video videoEntity1 = new Video();
//        Video videoEntity2 = new Video();
//
//        //3给第一个实体赋值，之后实体赋值类似即可
//        //3.1给章节名赋值
//        videoEntity0.setChapter("导论");
//
//        //3.2给courseVideoList插入map
//        List<Map<String, Object>> courseVideoList = new ArrayList<>();
//        Map<String, Object> videoMap01 = new HashMap<String, Object>();
//        Map<String, Object> videoMap02 = new HashMap<String, Object>();
//        Map<String, Object> videoMap03 = new HashMap<String, Object>();
//        Map<String, Object> videoMap04 = new HashMap<String, Object>();
//        videoMap01.put("videoName", "华为文化导论1");
//        videoMap01.put("videoPath", "https://www.bilibili.com/video/BV12Q4y1A7yr?from=search&seid=5507942461195094088");
//        courseVideoList.add(videoMap01);
//
//        videoMap02.put("videoName", "华为文化导论2");
//        videoMap02.put("videoPath", "https://www.bilibili.com/video/BV12Q4y1A7yr?from=search&seid=5507942461195094088");
//        courseVideoList.add(videoMap02);
//
//        //3.3，给单个实体的videoList赋值
//        videoEntity0.setCourseVideoList(courseVideoList);
//
//        //4.1给章节名赋值
//        videoEntity1.setChapter("第一章");
//
//        //4.2给courseVideoList插入map
//        List<Map<String, Object>> courseVideoList1 = new ArrayList<>();
//        Map<String, Object> videoMap11 = new HashMap<String, Object>();
//        Map<String, Object> videoMap12 = new HashMap<String, Object>();
//        Map<String, Object> videoMap13 = new HashMap<String, Object>();
//        Map<String, Object> videoMap14 = new HashMap<String, Object>();
//        videoMap11.put("videoName", "华为文化起源1");
//        videoMap11.put("videoPath", "https://www.bilibili.com/video/BV12Q4y1A7yr?from=search&seid=5507942461195094088");
//        courseVideoList1.add(videoMap11);
//
//        videoMap12.put("videoName", "华为文化起源2");
//        videoMap12.put("videoPath", "https://www.bilibili.com/video/BV12Q4y1A7yr?from=search&seid=5507942461195094088");
//        courseVideoList1.add(videoMap12);
//
//        videoMap13.put("videoName", "华为文化起源3");
//        videoMap13.put("videoPath", "https://www.bilibili.com/video/BV12Q4y1A7yr?from=search&seid=5507942461195094088");
//        courseVideoList1.add(videoMap12);
//
//        //4.3，给单个实体的videoList赋值
//        videoEntity1.setCourseVideoList(courseVideoList1);
//
//        //最后，将单个实体添加到返回体列表里
//        videoList.add(videoEntity0);
//        videoList.add(videoEntity1);
//        System.out.println("返回体列表为：\n" + videoList);
//        String totalvideoListJsonStr = toJSONString(videoList);//借助json工具类1，将对象List转换成json文本
//        System.out.println("返回体列表转换为json文本：\n" + totalvideoListJsonStr);
//
//        //json字符串转化为原来的列表
//        Class clazz_ReturnVideo = Video.class;
//        List<Video> changeVideoList = jsonStrToList(totalvideoListJsonStr, clazz_ReturnVideo);
//        System.out.println("json文本转化为列表：\n" + changeVideoList);
//
//
////       3的方法
//        List<Video> changeVideoList4 = stringToList(totalvideoListJsonStr, clazz_ReturnVideo);
//        System.out.println("utils3的json文本转化为列表：\n" + changeVideoList4);
//        videoEntity2.setChapter("导论");
////        returnVideoEntity2.setCourseVideoList(changeVideoList4);
//        videoList.add(videoEntity2);
//        System.out.println("最后的返回体列表为：\n" + videoList);
//        String videoListJsonStr555 = toJSONString(videoList);//借助json工具类1，将对象List转换成json文本
//        System.out.println("返回体列表的第一个实体转换为json文本：\n" + videoListJsonStr555);
////        System.out.println(apiStr);

//        ReturnStudyCourseDetails returnStudyCourseDetailsEntity = new ReturnStudyCourseDetails();
//        List<Chapter> chapterList = new ArrayList<>();
//        Chapter chapter0 = new Chapter("导论","0",1);
//        Chapter chapter1 = new Chapter("第一章","10",1);
//        Chapter chapter2 = new Chapter("第二章","20",1);
//        chapterList.add(chapter0);
//        chapterList.add(chapter1);
//        chapterList.add(chapter2);
//        String chapterStr = toJSONString(chapterList);
//        System.out.println("list转化成json字符串，chapterJsonStr="+chapterStr);
//
//        List<Notice> noticeList = new ArrayList<>();
//        Notice notice1 = new Notice("第一章视频将在今晚发布","2021-03-15 16:24:39","各位学员：今晚将在线上直播，敬请期待！",1);
//        Notice notice2 = new Notice("第二章视频将在明晚发布","2021-03-17 13:34:36","各位学员：明晚将在线上直播，敬请期待！",2);
//        noticeList.add(notice1);
//        noticeList.add(notice2);
//        String noticeStr = toJSONString(noticeList);
//        System.out.println("list转化成json字符串，noticeJsonStr="+noticeStr);
//
//        List<Section> sectionList = new ArrayList<>();
//        Section section0 = new Section("华为文化导论","1","华为文化导论.video","","video","0");
//        Section section1 = new Section("1.1华为文化起源","11","华为文化起源.video","","video","1");
//        Section section2 = new Section("1.2华为文化崛起","12","华为文化崛起.video","","video","1");
//        Section section3 = new Section("2.1华为文化发展中","21","华为文化发展中.video","","video","2");
//        sectionList.add(section0);
//        sectionList.add(section1);
//        sectionList.add(section2);
//        sectionList.add(section3);
//        String sectionStr = toJSONString(sectionList);
//        System.out.println("list转化成json字符串，sectionJsonStr="+sectionStr);
//
//        List<Resource> resourceList = new ArrayList<>();
//        Resource resource0 = new Resource("华为文化导论.pdf","pdf","");
//        Resource resource1 = new Resource("华为文化起源.pdf","docx","");
//        Resource resource2 = new Resource("华为文化崛起.docx","docx","");
//        Resource resource3 = new Resource("华为文化未来展望.ppt","ppt","");
//        resourceList.add(resource0);
//        resourceList.add(resource1);
//        resourceList.add(resource2);
//        resourceList.add(resource3);
//        String resourceStr = toJSONString(resourceList);
//        System.out.println("list转化成json字符串，resourceJsonStr="+resourceStr);
//
//
//
//        Class clazz_Notice= Notice.class;
//        List<Notice> noticeList2 =jsonStrToList(noticeStr,clazz_Notice);
//        System.out.println("jsonStr转化成list，list="+noticeList2);
//
//
//        returnStudyCourseDetailsEntity.setNoticeList(noticeList);


    }

}