package com.example.fruitsale.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Card {
    private Integer cardId;

    private String cardUid;

    private String openid;

    private Integer isBind;

    private Float cardMoney;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    public Card(Integer cardId, String cardUid, String openid, Integer isBind, Float cardMoney, Date gmtCreate, Date gmtModified) {
        this.cardId = cardId;
        this.cardUid = cardUid;
        this.openid = openid;
        this.isBind = isBind;
        this.cardMoney = cardMoney;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public Card() {
        super();
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getCardUid() {
        return cardUid;
    }

    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Integer getIsBind() {
        return isBind;
    }

    public void setIsBind(Integer isBind) {
        this.isBind = isBind;
    }

    public Float getCardMoney() {
        return cardMoney;
    }

    public void setCardMoney(Float cardMoney) {
        this.cardMoney = cardMoney;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}