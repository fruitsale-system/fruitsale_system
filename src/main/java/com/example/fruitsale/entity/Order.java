package com.example.fruitsale.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Order {
    private Integer orderId;

    private Integer userId;

    private String cardUid;

    private Integer buyWay;

    private Integer payWay;

    private String fruitName;

    private Float fruitWeight;

    private Float unitPrice;

    private Float totalPrice;

    private Integer payState;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    private String fruitPicture;

    public Order(Integer orderId, Integer userId, String cardUid, Integer buyWay, Integer payWay, String fruitName, Float fruitWeight, Float unitPrice, Float totalPrice, Integer payState, Date gmtCreate, Date gmtModified, String fruitPicture) {
        this.orderId = orderId;
        this.userId = userId;
        this.cardUid = cardUid;
        this.buyWay = buyWay;
        this.payWay = payWay;
        this.fruitName = fruitName;
        this.fruitWeight = fruitWeight;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.payState = payState;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
        this.fruitPicture = fruitPicture;
    }

    public Order() {
        super();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCardUid() {
        return cardUid;
    }

    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    public Integer getBuyWay() {
        return buyWay;
    }

    public void setBuyWay(Integer buyWay) {
        this.buyWay = buyWay;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public Float getFruitWeight() {
        return fruitWeight;
    }

    public void setFruitWeight(Float fruitWeight) {
        this.fruitWeight = fruitWeight;
    }

    public Float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getPayState() {
        return payState;
    }

    public void setPayState(Integer payState) {
        this.payState = payState;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getFruitPicture() {
        return fruitPicture;
    }

    public void setFruitPicture(String fruitPicture) {
        this.fruitPicture = fruitPicture;
    }
}