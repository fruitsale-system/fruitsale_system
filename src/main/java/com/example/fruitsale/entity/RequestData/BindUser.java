package com.example.fruitsale.entity.RequestData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BindUser {
    private Integer userId;
    private String cardUid;
}
