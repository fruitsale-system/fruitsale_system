package com.example.fruitsale.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChargeRecord {
    private Integer chargeId;

    private String cardUid;

    private Integer userId;

    private String nickname;

    private Integer chargeWay;

    private Float chargeMoney;

    private Float cardMoney;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date chargeTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    public ChargeRecord(Integer chargeId, String cardUid, Integer userId, String nickname, Integer chargeWay, Float chargeMoney, Float cardMoney, Date chargeTime, Date gmtCreate, Date gmtModified) {
        this.chargeId = chargeId;
        this.cardUid = cardUid;
        this.userId = userId;
        this.nickname = nickname;
        this.chargeWay = chargeWay;
        this.chargeMoney = chargeMoney;
        this.cardMoney = cardMoney;
        this.chargeTime = chargeTime;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public ChargeRecord() {
        super();
    }

    public Integer getChargeId() {
        return chargeId;
    }

    public void setChargeId(Integer chargeId) {
        this.chargeId = chargeId;
    }

    public String getCardUid() {
        return cardUid;
    }

    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getChargeWay() {
        return chargeWay;
    }

    public void setChargeWay(Integer chargeWay) {
        this.chargeWay = chargeWay;
    }

    public Float getChargeMoney() {
        return chargeMoney;
    }

    public void setChargeMoney(Float chargeMoney) {
        this.chargeMoney = chargeMoney;
    }

    public Float getCardMoney() {
        return cardMoney;
    }

    public void setCardMoney(Float cardMoney) {
        this.cardMoney = cardMoney;
    }

    public Date getChargeTime() {
        return chargeTime;
    }

    public void setChargeTime(Date chargeTime) {
        this.chargeTime = chargeTime;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}