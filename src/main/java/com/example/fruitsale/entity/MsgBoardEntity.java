package com.example.fruitsale.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "信息表实体", description = "Msg Entity")
public class MsgBoardEntity {

    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "作者")
    private String author;
    @ApiModelProperty(value = "发送的信息")
    private String msg;
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

}
