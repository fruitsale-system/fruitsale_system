package com.example.fruitsale.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class UserManage {
    private Integer userId;

    private String openid;

    private String nickname;

    private String realname;

    private String headImage;

    private Integer gender;

    private String cardUid;

    private Float cardMoney;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    private String sessionKey;

    private String skey;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date lastVisitTime;

    public UserManage(Integer userId, String openid, String nickname, String realname, String headImage, Integer gender, String cardUid, Float cardMoney, Date gmtCreate, Date gmtModified, String sessionKey, String skey, Date lastVisitTime) {
        this.userId = userId;
        this.openid = openid;
        this.nickname = nickname;
        this.realname = realname;
        this.headImage = headImage;
        this.gender = gender;
        this.cardUid = cardUid;
        this.cardMoney = cardMoney;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
        this.sessionKey = sessionKey;
        this.skey = skey;
        this.lastVisitTime = lastVisitTime;
    }

    public UserManage() {
        super();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getCardUid() {
        return cardUid;
    }

    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    public Float getCardMoney() {
        return cardMoney;
    }

    public void setCardMoney(Float cardMoney) {
        this.cardMoney = cardMoney;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public Date getLastVisitTime() {
        return lastVisitTime;
    }

    public void setLastVisitTime(Date lastVisitTime) {
        this.lastVisitTime = lastVisitTime;
    }
}