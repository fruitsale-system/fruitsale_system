package com.example.fruitsale.service;

import com.example.fruitsale.entity.UserManage;

public interface UserService {
    UserManage getById(Integer userId);

    int addUser(UserManage entity);

    Boolean updateById(Integer userId, UserManage entity);

    UserManage getByOpenid(String openid);

    int getLastId();

    int getUseridByOpenid(String openid);

    UserManage getUserBySkey(String skey);
}
