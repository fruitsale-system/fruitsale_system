package com.example.fruitsale.service;

import com.example.fruitsale.entity.MsgBoardEntity;

import java.util.List;

public interface IndexService {

    List<MsgBoardEntity> listAll();

    Boolean addMsg(MsgBoardEntity entity);

    Boolean delete(long id);

    Boolean update(long id, String msg);

    List<MsgBoardEntity> getListByauthor(String author);
}
