package com.example.fruitsale.service.impl;

import com.example.fruitsale.entity.Card;
import com.example.fruitsale.mapper.CardMapper;
import com.example.fruitsale.mapper.UserManageMapper;
import com.example.fruitsale.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CardService")
public class CardServiceImpl implements CardService {
    @Autowired
    CardMapper cardMapper;

    @Override
    public int cardBindUser(Card entity) {
        int flag = cardMapper.insertSelective(entity);
        return flag;
    }

    @Override
    public List<Card> getByOpenid(String openid) {
        List<Card> entities = cardMapper.getCardByOpenid(openid);
        return entities;

    }

    @Override
    public Card getByCarduid(String cardUid) {
        Card entity = cardMapper.getCardByUid(cardUid);
        return entity;
    }

    @Override
    public Boolean unbindUser(Card entity) {
        int flag = cardMapper.updateByPrimaryKeySelective(entity);
        if(flag == 1){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public int updateCard(Card entity) {
        int flag = cardMapper.updateByPrimaryKeySelective(entity);
        return flag;
    }

    @Override
    public List<Card> getMyCardsByUserid(Integer userId) {
        List<Card> cardEntities = cardMapper.getMyCardsByUserid(userId);
        return  cardEntities;
    }

    @Override
    public Integer getUseridByCarduid(String cardUid) {
        Integer userId = cardMapper.getUseridByCarduid(cardUid);
        return userId;
    }
}
