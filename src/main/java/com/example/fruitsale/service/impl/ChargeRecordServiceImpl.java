package com.example.fruitsale.service.impl;

import com.example.fruitsale.entity.ChargeRecord;
import com.example.fruitsale.mapper.ChargeRecordMapper;
import com.example.fruitsale.service.ChargeRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ChargeRecordService")
public class ChargeRecordServiceImpl implements ChargeRecordService {
    @Autowired
    ChargeRecordMapper chargeRecordMapper;

    @Override
    public int addChargeRecord(ChargeRecord entity) {
        int flag = chargeRecordMapper.insertSelective(entity);
        return flag;
    }

    @Override
    public List<ChargeRecord> getMyChargeRecords(Integer userId) {
        List<ChargeRecord> entities = chargeRecordMapper.getMyChargeRecords(userId);
        return entities;
    }

    @Override
    public List<ChargeRecord> getCardChargeRecords(String cardUid) {
        List<ChargeRecord> entities = chargeRecordMapper.getCardChargeRecords(cardUid);
        return entities;
    }
}
