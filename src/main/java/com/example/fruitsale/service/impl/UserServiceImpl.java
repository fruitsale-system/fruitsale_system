package com.example.fruitsale.service.impl;

import com.example.fruitsale.entity.UserManage;
import com.example.fruitsale.mapper.UserManageMapper;
import com.example.fruitsale.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("UserService")
public class UserServiceImpl implements UserService {
    @Autowired
    UserManageMapper userManageMapper;

    @Override
    public UserManage getById(Integer userId) {
        UserManage entity = userManageMapper.selectByPrimaryKey(userId);
        return entity;
    }

    @Override
    public int addUser(UserManage entity) {
        int flag = userManageMapper.insertSelective(entity);
        return flag;
    }

    @Override
    public Boolean updateById(Integer userId, UserManage entity) {
        int flag = userManageMapper.updateByPrimaryKeySelective(entity);
        if(flag == 1){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public UserManage getByOpenid(String openid) {
        UserManage entity = userManageMapper.getByOpenid(openid);
        return entity;
    }

    @Override
    public int getLastId() {
        int lastId = userManageMapper.getLastId();
        return lastId;
    }

    @Override
    public int getUseridByOpenid(String openid) {
        int userId = userManageMapper.getUseridByOpenid(openid);
        return userId;
    }

    @Override
    public UserManage getUserBySkey(String skey) {
        UserManage entity = userManageMapper.getUserBySkey(skey);
        return entity;
    }

}
