package com.example.fruitsale.service.impl;

import com.example.fruitsale.entity.MsgBoardEntity;
import com.example.fruitsale.mapper.IndexMapper;
import com.example.fruitsale.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("IndexService")
public class IndexServiceImpl implements IndexService {

    @Autowired
    IndexMapper indexMapper;

    @Override
    public List<MsgBoardEntity> listAll() {
        List<MsgBoardEntity> entities = indexMapper.list();
        return entities;
    }

    @Override
    public Boolean addMsg(MsgBoardEntity entity) {
        entity.setGmtCreate (new Date());//创建的时间就不用用户输入了，后端自动获取系统时间
        Boolean b = indexMapper.save(entity);

        return b;
    }

    @Override
    public Boolean delete(long id) {
        Boolean b = indexMapper.delete(id);
        return b;
    }

    @Override
    public Boolean update(long id, String msg) {
        Boolean b = indexMapper.update(id,msg);
        return b;
    }

    @Override
    public List<MsgBoardEntity> getListByauthor(String author) {
        List<MsgBoardEntity> entities = indexMapper.getListByauthor(author);
        return entities;
    }
}
