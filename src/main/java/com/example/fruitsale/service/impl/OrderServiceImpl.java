package com.example.fruitsale.service.impl;

import com.example.fruitsale.entity.Order;
import com.example.fruitsale.mapper.OrderMapper;
import com.example.fruitsale.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("OrderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public int payOrder(Order entity) {
        int flag = orderMapper.insert(entity);
        return flag;
    }

    @Override
    public Order getById(Integer id) {
        Order entity = orderMapper.selectByPrimaryKey(id);
        return entity;
    }


    @Override
    public List<Order> getOrderByUseridAndBuyway(Integer userId, Integer buyWay) {
        List<Order> entities = orderMapper.getOrderByUseridAndBuyway(userId,buyWay);
        return entities;
    }

    @Override
    public List<Order> getOrderByUseridAndPayway(Integer userId, Integer payWay, Integer buyWay) {
        List<Order> entities = orderMapper.getOrderByUseridAndPayway(userId,payWay,buyWay);
        return entities;
    }

    @Override
    public List<Order> getOrderByCarduid(String cardUid) {
        List<Order> entities = orderMapper.getOrderByCarduid(cardUid);
        return entities;
    }
}
