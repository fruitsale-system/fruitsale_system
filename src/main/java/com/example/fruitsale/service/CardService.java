package com.example.fruitsale.service;

import com.example.fruitsale.entity.Card;

import java.util.List;

public interface CardService {
    int cardBindUser(Card entity);

    List<Card> getByOpenid(String openid);

    Card getByCarduid(String cardUid);

    Boolean unbindUser(Card entity);

    int updateCard(Card entity);

    List<Card> getMyCardsByUserid(Integer userId);

    Integer getUseridByCarduid(String cardUid);
}
