package com.example.fruitsale.service;

import com.example.fruitsale.entity.Order;

import java.util.List;

public interface OrderService {
    int payOrder(Order entity);

    Order getById(Integer id);


    List<Order> getOrderByUseridAndBuyway(Integer userId, Integer buyWay);

    List<Order> getOrderByUseridAndPayway(Integer userId, Integer payWay, Integer buyWay);

    List<Order> getOrderByCarduid(String cardUid);
}
