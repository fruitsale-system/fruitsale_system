package com.example.fruitsale.service;

import com.example.fruitsale.entity.ChargeRecord;
import com.example.fruitsale.entity.UserManage;

import java.util.List;

public interface ChargeRecordService {
    int addChargeRecord(ChargeRecord entity);

    List<ChargeRecord> getMyChargeRecords(Integer userId);

    List<ChargeRecord> getCardChargeRecords(String cardUid);
}
